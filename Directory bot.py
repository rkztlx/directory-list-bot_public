import os
import sys
import json
import requests
from datetime import date

# Insert telegram botTokenId
botTokenID = ""  

# Insert telegram ChannetChatId
ChannelChatID = "" #

# Create dictionary with key the folders in dir and value the path + namefile of the file with the extension founded in subfolders of dir 
def list_directory_items(dir, extension,database):
    database_new = database

    # take all key
    folder_keys = os.listdir(dir)
    
    # root: root path
    # dirs: subfolder in current dir
    # files: files in current dir 
    for (root,dirs,files) in os.walk(dir):
        for file in files:
            if file.endswith(extension):
                for folder in folder_keys:
                    # Known bug don't use similar name in different folders 
                    # example test and test2
                    if folder in root:
                        database_new[folder].append(f'{root}/{file}')
    return database_new

# Check if are present new items since last scan, and notify if them were deleted or added
def check_new_items(dir,extension,database_old):
    database_new = create_list_files_empty(dir)
    items_new = list_directory_items(dir, extension, database_new).items()
    items_old = database_old.items()
    items_to_notify = []

    for (key_new, value_new) in items_new:
        if value_new != database_old[key_new]:
            for value in value_new:
                if value not in database_old[key_new]:
                    # this function was added for personal use, fill free to modify it with your use-case
                    items_to_notify.append(f'[Added] {value.split(key_new)[1].split(extension)[0].replace("."," ")} in the category {key_new}')
    
    for (key_old, value_old) in items_old:
        if value_old != database_new[key_old]:
            for value in value_old:
                if value not in database_new[key_old]:
                    items_to_notify.append(f'[Removed] {value.split(key_old)[1].split(extension)[0].replace("."," ")} in the category {key_old}')

    # send message to telegram channel throw api and log message to file for future debug
    with open("log.txt", "a+") as log_file:
        for message in items_to_notify:
            log_file.write(f'[{date.today()}] {message}\n')
            url = f"https://api.telegram.org/bot{botTokenID}/sendMessage?chat_id={ChannelChatID}&text={message}"
            r = requests.get(url)

    # overwrite the old database dictionary with the new one
    with open("database.json", "w+") as input_json:
        json.dump(database_new,input_json)
    
# Create empty dictionary with key folders in current dir
def create_list_files_empty(dir):
    database = {}
    folder_keys = os.listdir(dir)
    for folder in folder_keys:
        if os.path.isdir(f'{dir}/{folder}'):
            database[folder] = []
    return database

def main():
    dir = sys.argv[1]
    extension = sys.argv[2]

    # if database doesn't exist create it and exit
    if not os.path.exists('database.json'):
        with open("database.json", "w+") as input_json:
            database = create_list_files_empty(dir)
            database = list_directory_items(dir, extension, database)
            json.dump(database,input_json)
            return
    database = {}
    # if the database allready exist check for diff since last scan
    with open("database.json", "r") as input_json:
        database = json.load(input_json)
        check_new_items(dir,extension,database)

main()