# Directory List Bot 
List all files in a directory with a given extension and send a message to a telegram channel using a telegram bot if a file was removed or added since last scan

## Getting started

### Installation
Install the packages needed in requirements.txt with:
    `pip install requirements.txt`

Variables to edit to run it the first time

| Variable | Purpose |
| ------ | ------ |
| `botTokenID` | Telegram Bot Token ID, it can be retrieven from god father bot|
| `ChannelChatID` | Telegram Channel Chat ID, it can be retrieven from channel page under title|

Edit `shell_script_sh` with the arguments that will be passed to the program

| Variable | Purpose |
| ------ | ------ |
|`<path-dir>`| Path from where to scan the items, put `.` if you want current dir|
|`extension`| File extension of files, example `.txt`|

First time make the `shell_script.sh` executable with:
    `chmod +x shell_script.sh`

### Usage
Launch the script `shell_script.sh` executable with:
    `./shell_script.sh`

### Project status
All code was written in few nights so it is not optimized but works, only for educational purpose, don't use it for path traversal attack please :)
